#FitsViewerLight
============
Quick and light Application in Python for show Fits Image or Spectrum in a plot (matplotlib).
More info at : https://stellartrip.net/visualiseur-fits-light-python/

|python| |dependencies| |licence|

Installation - FitsViewerLight
------------

Astropy : https://www.astropy.org/

Specutils https://specutils.readthedocs.io/en/stable/spectrum1d.html

Numpy https://numpy.org/

Matplotlib https://matplotlib.org/

Style Cyberpunk https://github.com/dhaitz/mplcyberpunk

(use requirements.txt for a quick install with pip)

Features - FitsViewerLight
--------

- Open a Fits Image
- Open a Spectrum File 
- Enable/Disbale Second Axis for Spectrum Plot
- Enable/Disbale Cyberpunk Style for Spectrum Plot
- Show FITS Header
- Modify Fits Header


Screen
--------
Base style
    |fvlimg|

Cyberpunk Style
    |fvlimgcbpk|




Usage - FitsViewerLight
--------

- For Launch Application 
``$ cd src``
``$ python3 main.py``

- For open a FITS Image 
Drag & Drop the file on Application
Or open file with the menu "File > Open file" 

- For open a FITS Spectrum 
Drag & Drop the file on Application
Or open file with the menu "File > Open spectrum" 



.. |python| image:: https://img.shields.io/badge/Python-3.7%203.8-green
    :alt: Python
    :scale: 100%
    :target: https://www.python.org/

.. |dependencies| image:: https://img.shields.io/badge/dependencies-astropy-orange
    :alt: Dependencies
    :scale: 100%
    :target: https://www.astropy.org/

.. |licence| image:: https://img.shields.io/badge/licence-GPL%203.0-orange
    :alt: Licence gpl-v3.0
    :scale: 100%
    :target: https://www.gnu.org/licenses/gpl-3.0.fr.html

.. |fvlimg| image:: https://stellartrip.net/wp-content/uploads/2020/10/fvl_stellartrip_little.png
    :alt: FitsViewerLight Screen
    :scale: 100%
    :target: https://stellartrip.net/wp-content/uploads/2020/10/fvl_stellartrip.png

.. |fvlimgcbpk| image:: https://stellartrip.net/wp-content/uploads/2020/10/fvl_cybpk_style_little.png
    :alt: FitsViewerLight Screen
    :scale: 100%
    :target: https://stellartrip.net/wp-content/uploads/2020/10/fvl_cybpk_style.png