##########
# IMPORT #
##########
from PySide2 import QtWidgets
from PySide2.QtWidgets import*

class closePicture(QWidget):
    """Controller of the Close picture button. Reset the software
    
    '''

    Attibutes
    ----------
    parent : MyWindow
        Window containing the image and data to be reset

    '''

    Methods
    -------
    close()
        Closes the image and resets the displayed data and statistics
    """

    def __init__(self, parent):
        """
        Parameters
        ----------
        parent : MyWindow
            Window containing the image and data to be reset
        """
        self.parent = parent

    def close(self):
        """Closes the image and resets the displayed data and statistics"""

        # closing the photo and graphic
        embedMPLgraph = self.parent.ui.findChildren(QtWidgets.QVBoxLayout, 'embedMPLgraph')[0]
        for i in reversed(range(embedMPLgraph.count())):
            embedMPLgraph.itemAt(i).widget().setParent(None)

        embedMPL = self.parent.ui.findChildren(QtWidgets.QVBoxLayout, 'embedMPL')[0]
        for i in reversed(range(embedMPL.count())):
            embedMPL.itemAt(i).widget().setParent(None)

        # reset of displayed data and statistics to 0
        tab = self.parent.ui.findChildren(QtWidgets.QTableWidget, 'tableWidget')[0]
        while tab.rowCount() > 0:
            tab.removeRow(0)

        label = self.parent.ui.findChildren(QtWidgets.QLabel, 'minPixVal')[0]
        label.setText(str(0))
        label = self.parent.ui.findChildren(QtWidgets.QLabel, 'minPixVal')[0]
        label.setText(str(0))
        label = self.parent.ui.findChildren(QtWidgets.QLabel, 'avgPixVal')[0]
        label.setText(str(0))
        label = self.parent.ui.findChildren(QtWidgets.QLabel, 'maxPixVal')[0]
        label.setText(str(0))
        label = self.parent.ui.findChildren(QtWidgets.QLabel, 'minPixSel')[0]
        label.setText(str(0))
        label = self.parent.ui.findChildren(QtWidgets.QLabel, 'avgPixSel')[0]
        label.setText(str(0))
        label = self.parent.ui.findChildren(QtWidgets.QLabel, 'maxPixSel')[0]
        label.setText(str(0))
        
        self.parent.ui.findChildren(QtWidgets.QLabel, 'spDate')[0].setText(str(0))
        self.parent.ui.findChildren(QtWidgets.QLabel, 'spSiteLat')[0].setText(str(0))
        self.parent.ui.findChildren(QtWidgets.QLabel, 'spSiteLong')[0].setText(str(0))
        self.parent.ui.findChildren(QtWidgets.QLabel, 'spTelescop')[0].setText(str(0))
        self.parent.ui.findChildren(QtWidgets.QLabel, 'spInstrument')[0].setText(str(0))
        self.parent.ui.findChildren(QtWidgets.QLabel, 'spFilter')[0].setText(str(0))
        self.parent.ui.findChildren(QtWidgets.QLabel, 'spExposure')[0].setText(str(0))
        self.parent.ui.findChildren(QtWidgets.QLabel, 'spBitPix')[0].setText(str(0))

        menuOpened_file = self.parent.ui.findChildren(QtWidgets.QMenu, 'menuOpened_file')[0]
        menuOpened_file.setTitle("")

        button = self.parent.ui.findChildren(QtWidgets.QPushButton, 'pushButton')[0]
        button.setEnabled(False)
