##########
# IMPORT #
##########

from PySide2 import QtWidgets
from astropy.io import fits
from matplotlib.figure import Figure
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import (NavigationToolbar2QT as NavigationToolbar)
from matplotlib.widgets import Cursor, RectangleSelector
from numpy import arange, amin, amax, average
from utils.imageEditor import ImageEditor
import tempfile
import re
import os
import shutil

class FitsManager:
    """Class that manages FITS/FIT files in order to be able to easily manipulate the data and display the photo.

    '''

    Attributes
    ----------
    cursor1 Cursor
        Crosshairs of the image
    cursor2 Cursor
        Crosshairs of the histogram
    rs1
        Selecting the image
    rs2
        Selecting the histogram
    headerEntryCount int
        counter of the number of fields of the header of the open file
    openedFile str
        path of the open file

    '''

    Methods
    -------
    loadFitsFile(filePath, ui)
        Retrieves the data present in the header and displays the photograph in the software
    onclick(event):
        Updates the cursor position on the image when moving
    editFitsFileHeader(datalist,ui)
        Saves the changes made to the header in the open fits file.
    """

   
    headerEntryCount = 0
    openedFile = ""
    

    @staticmethod
    def loadFitsFile(filePath, ui):
        """Retrieves the data present in the header and displays the photograph in the software

        Parameters
        ----------
        filePath : str
            contains the path to the file to be loaded
        ui : MyWindow
            the software interface where the data and the photograph will be displayed
        """
        FitsManager.openedFile = filePath;

        embedMPL = ui.findChildren(QtWidgets.QVBoxLayout, 'embedMPL')[0]
        ui.findChildren(QtWidgets.QTabWidget, 'left')[0].setTabText(0, "Image")

        for i in reversed(range(embedMPL.count())):
            embedMPL.itemAt(i).widget().setParent(None)

        image_data = fits.getdata(filePath)



        fig = Figure(figsize=(4, 4), dpi=100)
        ax = fig.add_subplot(111)
        ax.imshow(image_data, cmap='gray')
        ax.set_xlabel("Pixels")
        ax.set_ylabel("Pixels")

        canvas = FigureCanvas(fig)
        embedMPL.addWidget(canvas)
        embedMPL.addWidget(NavigationToolbar(canvas, canvas))
        cursor = Cursor(ax, useblit=False, color='white', linewidth=1)

        updatefits_btn = ui.findChildren(QtWidgets.QPushButton, 'updatefits_btn')[0]
        updatefits_btn.setEnabled(True)

        ImageEditor.imageLoad(image_data, ax, ui, fig)


        hdul = fits.open(filePath) #Retrieving the file path
        data = hdul[0].data #Recovery of file data

        # Retrieving the header and inserting it into the table
        table = ui.findChildren(QtWidgets.QTableWidget, 'tableWidget')[0]

        table.setRowCount(0)

        header = fits.getheader(filePath)
        i = 0
        for line in header:
            table.insertRow(table.rowCount())
            table.setItem(table.rowCount() - 1, 0, QtWidgets.QTableWidgetItem(line))
            table.setItem(table.rowCount() - 1, 1, QtWidgets.QTableWidgetItem(str(header[i])))
            i = i + 1
        FitsManager.headerEntryCount = i; # Save the number of entries in the header


        #Print path file label in status bar
        pathFileValueLabel = ui.findChildren(QtWidgets.QLabel, 'path_file_value_label')[0]
        pathFileValueLabel.setText(FitsManager.openedFile)

        #Print minimum pixel value in status bar
        min_pixel_value_label = ui.findChildren(QtWidgets.QLabel, 'min_pixel_value_label')[0]
        min_pixel_value_label.setText(str(round(amin(data),4)))

        #Print average pixel value in status bar
        avg_pixel_value_label = ui.findChildren(QtWidgets.QLabel, 'avg_pixel_value_label')[0]
        avg_pixel_value_label.setText(str(round(average(data), 4)))

        #Print maximum pixel value in status bar
        max_pixel_value_label = ui.findChildren(QtWidgets.QLabel, 'max_pixel_value_label')[0]
        max_pixel_value_label.setText(str(round(amax(data),4)))


        
    def editFitsFileHeader(datalist,ui):
        """Saves the changes made to the header of the open fits file.
        
        Parameters
        ----------
        datalist : QTableWidget
            table containing all the header data.
        ui : MyWindow
            Window displaying the photograph and header information. Will be updated after the changes made in the header.
        """
        fileName = re.search(r"(/|\\)(\b[^\\/]*\b)(\.fits|\.fit)", FitsManager.openedFile).group(2)  # Retrieve the name of the loaded file
        fileExt = re.search(r"(/|\\)(\b[^\\/]*\b)(\.fits|\.fit)", FitsManager.openedFile).group(3)  # Retrieve the extension of the loaded file
        temp = tempfile.NamedTemporaryFile(mode="w", prefix=fileName + str("-"), suffix=fileExt, delete=False)  # Create a temporary file to save to store modified file, modifier to keep file even if closed, name with the original name of the file, a random string, and it's extension
        temp.close() # Close the temporary file

        file = fits.open(FitsManager.openedFile, mode='update')
        for i in range(0, FitsManager.headerEntryCount):
            value = datalist.item(i, 1).text()  # Retrieving the value of the list
            file[0].header[i] = type(file[0].header[i])(value)  # Assigning the list value to the header
        file[0].writeto(temp.name, overwrite=True)  # Writing the modified FITS file
        file.close()

        os.remove(FitsManager.openedFile) # Remove the opened fits file you edited
        shutil.move(temp.name,FitsManager.openedFile) # Move the temporary file to the path where the was the edited file
        FitsManager.loadFitsFile(FitsManager.openedFile,ui) # Load the new file which contain edited datas