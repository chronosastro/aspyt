#for last version mac os (thanks update...)
import os
os.environ["QT_MAC_WANTS_LAYER"] = "1"

#imports
import sys
from PySide2.QtWidgets import QApplication, QMainWindow,  QFileDialog
from ui_mainwindow import Ui_MainWindow
#add Web Engine and icon
from PySide2.QtWebEngineWidgets import QWebEngineView
from PySide2 import QtGui #add an icon ;)
#astro imports
import astropy.wcs as fitswcs #wcs
from specutils import Spectrum1D
from astropy.io import fits
import astropy.units as u
#import plotly
from plotly.graph_objects import Figure, Scatter
import plotly

#For windows usage
#import PySide2
##dirname = os.path.dirname(PySide2.__file__)
#plugin_path = os.path.join(dirname, 'plugins', 'platforms')
#os.environ['QT_QPA_PLATFORM_PLUGIN_PATH'] = plugin_path


class MainWindow(QMainWindow):
    def __init__(self):
        super(MainWindow, self).__init__()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        opn_btn = self.ui.open_file_btn
        opn_btn.clicked.connect(lambda : MainWindow.open_fits_spec(self))

        #Path file info
        self.path =  self.ui.file_path_label
        #Spectrum info
        self.spectrum_infos_label = self.ui.spectrum_infos_label
        #Layout for plot
        self.embedMPL = self.ui.mylayout


 #----------- Functions here -------#
    #Prepare a spec1d form a fits file
    def prepare_spec1d(self, fits_file_path):

        #open file
        f = fits.open(fits_file_path)
        
        #get data
        header = f[0].header
        data = f[0].data
        
        # Precise flux units
        flux = data * u.Jy
        
        #------ with WCS object -----#
        wcs_data = fitswcs.WCS(header={'CDELT1': header['CDELT1'], 'CRVAL1': header['CRVAL1'],
                                    'CUNIT1': header['CUNIT1'], 'CTYPE1': header['CTYPE1'],
                                    'CRPIX1': header['CRPIX1']})
        
        # Create a Spec1D object with specutils
        spec = Spectrum1D(flux=flux, wcs=wcs_data)
        
        return spec, header


    #Show a spec1d
    def show_spec1d(self, spec1d, header):
    
        # Figure init
        fig01, ax01 = plt.subplots(figsize=(16, 9))
        
       
        # Set data for plot
        ax01.plot(spec1d.spectral_axis * u.AA, spec1d.flux)
        
        # X axis title
        ax01.set_xlabel(header['CTYPE1'] + ' - ' + header['CUNIT1'])
        
        # Y axis title
        ax01.set_ylabel('Relative Flux')
        
        # Grid config
        ax01.grid(color='grey', alpha=0.8, linestyle='-.', linewidth=0.2, axis='both') 
            
        # Legend
        legend_value = header['OBJNAME'] + ' - ' + header['DATE-OBS']
        ax01.legend([legend_value], loc=('best'))
        
        
        # Create title with header data
        spectrumTitle = header['OBJNAME'] + ' - ' + header['DATE-OBS'] + ' - ' + header['EXPTIME2']+ ' - ' + str(header['DETNAM'] + ' - ' + header['OBSERVER'])
        ax01.set_title(spectrumTitle, loc='center', fontsize=14, fontweight=0.5)
        
        # Png record
        #plt.savefig('spectrum.png')
        
        # Show plot
        plt.show()
        print('Plot generated.')




    def open_fits_spec(self):
        #Clear widget before plot
        for i in reversed(range(self.embedMPL.count())): 
            self.embedMPL.itemAt(i).widget().deleteLater()

        print('Select a file')
        filename = QFileDialog.getOpenFileName(self)
        print(filename[0])

        #create spectrum
        sp, hd = self.prepare_spec1d(filename[0])

        # create the plotly figure
        fig = Figure(Scatter(x=sp.spectral_axis, y=sp.flux))

        # we create html code of the figure
        html = '<html><body>'
        html += plotly.offline.plot(fig, output_type='div', include_plotlyjs='cdn')
        html += '</body></html>'

        # we create an instance of QWebEngineView and set the html code
        plot_widget = QWebEngineView()
        plot_widget.setHtml(html)


        # add widget to my widget
        self.embedMPL.addWidget(plot_widget)
        
        self.path.setText(filename[0])
        self.spectrum_infos_label.setText(str(hd["OBJNAME"]) + " - " + str(hd["EXPTIME2"]) + " - " + str(hd["DATE-OBS"]) + " - " + str(hd["OBSERVER"]) + " - " +str(hd["BSS_SITE"]))


if __name__ == "__main__":
    app = QApplication(sys.argv)
    app.setWindowIcon(QtGui.QIcon('shelyak_logo.png'))
    window = MainWindow()
    window.show()

    sys.exit(app.exec_())