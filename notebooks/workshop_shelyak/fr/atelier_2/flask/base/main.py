from flask import Flask
from flask import render_template
from plotly import io

import betools

"""
Créons une application Flask simple

    ===== Objectif du script ===
    Principe de base d'une appli Flask
        - Préparer et afficher un spectre
        - Affichage avec matplotlib (fct)
    ==========================
    

    ===== Éléments abordés =====
    Route
    Template
    Intro Bootstrap : https://getbootstrap.com/docs/5.0/examples/
    URL static : {{ url_for('static', filename='sticky-footer.css') }}
    ============================
"""



#Définition de l'instance Flask
app = Flask(__name__)


#Route principale
@app.route('/')
def index():
    return '<b>Index Page</b>'


#Route hello
@app.route('/hello')
def hello():
    name_to_show ='Matthieu'
    return render_template('design.html', username = name_to_show)

#Route spectrum
@app.route('/spectrum') #,methods=['GET', 'POST'])
def spectrum():

    #Preparation du spec
    spec = betools.prepare_spec1d('data/28tau_20131215_916.fits')
    #Récupération de la figure
    fig_plotly = betools.get_plotly_spec1d(spec[0], spec[1], False)
    
    
    #-------- Génération d'un graph html/js à partir de notre objet figure précédent avec la méthode io.to_html de plotly ------#
    div_plot = io.to_html(fig_plotly, full_html=False, include_plotlyjs=True)


    #Envoi vers le template spectral.html de l'élément div_plot
    return render_template('spectra.html', plot = div_plot)



if __name__ == "__main__":
   app.run(debug=True) # Démarrage de l'application
   #app.run(host= '192.168.0.2')
 
    