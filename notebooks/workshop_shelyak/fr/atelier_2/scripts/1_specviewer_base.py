import sys

import astropy.units as u
from astropy.io import fits
import astropy.wcs as fitswcs #wcs
from specutils import Spectrum1D, SpectralRegion #spectrum1D (specutils)
from specutils.manipulation import extract_region
from astropy.modeling import models, fitting
from specutils.fitting import fit_generic_continuum

# Imports maths, data et visualisation
import matplotlib.pyplot as plt
from matplotlib import colors
import plotly.graph_objects as go
import plotly.express as px
import numpy as np


#Modification des imports pour pouvoir afficher le graph
from plotly.offline import download_plotlyjs, init_notebook_mode,  plot
from plotly.graph_objs import *
init_notebook_mode()

"""
Reprenons le code de notre premier atelier afin de simplement visualiser
un fichier de spectré déjà réduit, grâce à specutils et matoplotib :
    

    ===== Objectif du script ===
    Creation d'un spec1D
    Affichage avec matplotlib
    ==========================
    

    ===== Éléments abordés =====
    Organisation du layout
    Aide/Doc : Windows(CMD) + I
    Visualisation des variables
    Indicateur d'erreurs et de warning
    Points d'arrêts / Débuggage
    Sortie des graphes
    ============================
    

"""

#----- Préparation du spectre -----#
#open file
filepath_spectre='data/_28tau_20131215_916.fits'

f = fits.open(filepath_spectre)
f.info()


header = f[0].header
data = f[0].data
print(repr(header))


#Spec Preparation
# Précision des unités du flux
flux = data * u.Jy

# Avec le fitswcs
wcs_data = fitswcs.WCS(header={'CDELT1': header['CDELT1'], 'CRVAL1': header['CRVAL1'],
                               'CUNIT1': header['CUNIT1'], 'CTYPE1': header['CTYPE1'],
                               'CRPIX1': header['CRPIX1']})

# Création d'un objet Spectrum1D avec specutils
spec = Spectrum1D(flux=flux, wcs=wcs_data)


#---- Affichage----#

# Init de la figure
fig01, ax01 = plt.subplots(figsize=(16, 9))

# Définition des valeurs à afficher, X et Y
# On précise également les unités pour la longueur d'onde
ax01.plot(spec.spectral_axis * u.AA, spec.flux)

# Titre de l'axe X
ax01.set_xlabel(header['CTYPE1'] + ' - ' + header['CUNIT1'])

# Titre de l'axe Y
ax01.set_ylabel('Relative Flux')

# Configuration de la grille
ax01.grid(color='grey', alpha=0.8, linestyle='-.', linewidth=0.2, axis='both') 
    
# Légende
legend_value = header['OBJNAME'] + ' - ' + header['DATE-OBS']
ax01.legend([legend_value], loc=('best'))


# Creation d'un titre et ajout sur le graphique
spectrumTitle = header['OBJNAME'] + ' - ' + header['DATE-OBS'] + ' - ' + header['EXPTIME2']+ ' - ' + str(header['DETNAM'] + ' - ' + header['OBSERVER'])
ax01.set_title(spectrumTitle, loc='center', fontsize=14, fontweight=0.5)

# Enregistrement en png
#plt.savefig('spectrum.png')

# Affichage
plt.show()
