# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'mainwindow.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        if not MainWindow.objectName():
            MainWindow.setObjectName(u"MainWindow")
        MainWindow.resize(1492, 1080)
        self.centralWidget = QWidget(MainWindow)
        self.centralWidget.setObjectName(u"centralWidget")
        self.open_file_btn = QPushButton(self.centralWidget)
        self.open_file_btn.setObjectName(u"open_file_btn")
        self.open_file_btn.setGeometry(QRect(40, 20, 186, 51))
        self.open_file_btn.setStyleSheet(u"   background-color: rgb(0, 224, 118);\n"
"    border-style: outset;\n"
"    border-width: 2px;\n"
"    border-radius: 10px;\n"
"    border-color: beige;\n"
"    font: bold 14px;\n"
"    min-width: 10em;\n"
"    padding: 6px;")
        self.file_path_lbl = QLabel(self.centralWidget)
        self.file_path_lbl.setObjectName(u"file_path_lbl")
        self.file_path_lbl.setGeometry(QRect(260, 20, 1121, 51))
        self.mywidget = QWidget(self.centralWidget)
        self.mywidget.setObjectName(u"mywidget")
        self.mywidget.setGeometry(QRect(0, 90, 1491, 921))
        self.verticalLayoutWidget = QWidget(self.mywidget)
        self.verticalLayoutWidget.setObjectName(u"verticalLayoutWidget")
        self.verticalLayoutWidget.setGeometry(QRect(10, 0, 1471, 921))
        self.mylayout = QVBoxLayout(self.verticalLayoutWidget)
        self.mylayout.setSpacing(6)
        self.mylayout.setContentsMargins(11, 11, 11, 11)
        self.mylayout.setObjectName(u"mylayout")
        self.mylayout.setContentsMargins(0, 0, 0, 0)
        MainWindow.setCentralWidget(self.centralWidget)
        self.menuBar = QMenuBar(MainWindow)
        self.menuBar.setObjectName(u"menuBar")
        self.menuBar.setGeometry(QRect(0, 0, 1492, 24))
        MainWindow.setMenuBar(self.menuBar)
        self.mainToolBar = QToolBar(MainWindow)
        self.mainToolBar.setObjectName(u"mainToolBar")
        MainWindow.addToolBar(Qt.TopToolBarArea, self.mainToolBar)
        self.statusBar = QStatusBar(MainWindow)
        self.statusBar.setObjectName(u"statusBar")
        MainWindow.setStatusBar(self.statusBar)

        self.retranslateUi(MainWindow)

        QMetaObject.connectSlotsByName(MainWindow)
    # setupUi

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QCoreApplication.translate("MainWindow", u"MainWindow", None))
        self.open_file_btn.setText(QCoreApplication.translate("MainWindow", u"Open File", None))
        self.file_path_lbl.setText(QCoreApplication.translate("MainWindow", u"TextLabel", None))
    # retranslateUi

