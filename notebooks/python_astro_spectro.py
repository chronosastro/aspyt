#!/usr/bin/env python
# coding: utf-8

# # Python pour l'astronomie et la spectroscopie : Analyse rapide d'un spectre

# Ce notebook contient le code qui a servit à l'élaboration de l'article éponyme, disponible à cette adresse : https://stellartrip.net/python-pour-lastronomie-et-la-spectroscopie-analyse-rapide-dun-spectre/
# 
# Le jeu de données utilisé pour ce notebook est téléchargeable à cette adresse : http://e.pc.cd/2MhotalK
# 
# Auteur : MLA

# ## Coordonnées

# ### 1- Récupération des coordonnées de l'étoile gam Cas 

# #### 1.1 - Utilisation d'astropy - Coordinates
# import du module de de gestion des coorodnnées de la librairie Astropy

# In[1]:


#!pip install astropy
from astropy.coordinates import SkyCoord


# Un objet SkyCoords représente une position ICRS (Right ascension [RA], Declination [Dec]) dans le ciel.
# 
# Il est ainsi possible de récupérer la position de notre cible, en appelant la méthode from_name qui prend en paramètre le nom de l'objet, puis on affichage ensuite sa valeur.

# In[2]:


gam_cas_coords = SkyCoord.from_name("HD5394")
print(f"Objet SkyCoords : {gam_cas_coords}")


# À partir de cette variable, qui est un objet SkyCoord, il est possible d'accèder aux valeurs distinctes de l'ascension droite et de la déclinaison. Selon les méthodes utilisées, différents formats et finesses des données sont accessibles.

# In[3]:


print(f" - Position RA/DEC (hms & dms / ep=J2000): {gam_cas_coords.to_string('hmsdms')}")
print(f" - Position RA et DEC (ep=J2000) : {gam_cas_coords.ra.hms, gam_cas_coords.dec.dms}")
print(f" - RA (min) : {gam_cas_coords.ra.hms.m}")


# Dans l'autre sens, nous pouvons aussi créer un objet SkyCoord  à partir des coordonnées connues et de leur format. 

# In[4]:


from astropy import units as u
c = SkyCoord(14.184, 60.7249, frame='icrs', unit='deg')
c = SkyCoord('00h56m44.16s', '+60d43m29.64s', frame='icrs')
c = SkyCoord('00 56 44.16 +60 43 29.64', unit=(u.hourangle, u.deg))
c  


# #### 1.2 - Utilisation d'Astroquery et du module SIMBAD pour récupération des coordonnées et du type spectral de gam Cas
# 
# Par ailleurs, il est également possible d’interroger les différents catalogues avec la librairie Astroquery, comme dans l’exemple ci-dessous avec avec une requête vers SIMBAD8, qu’il s’agisse d’un objet du ciel ou d’une région entière (https://astroquery.readthedocs.io/en/latest/simbad/simbad.html). La liste des modules catalogues est disponible ici : https://astroquery.readthedocs.io/en/latest/
# 
# De plus, l’ajout du champ « sptype » dans la requête VOTable permet également de récupérer le type spectral de l’étoile. La liste des critères possibles est disponible ici.

# In[5]:


#!pip install astroquery

#import module
from astroquery.simbad import Simbad

#get a simbad instance
simbad = Simbad()

#add spectral type parameters for VOTable request
simbad.add_votable_fields('sptype')

#request
result_table = simbad.query_object("gam cas")
result_table.pprint(show_unit=True)

#Coordinates
print("\nCoordinates")
print(result_table['RA'][0])
print(result_table['DEC'][0])

#Spectral Type
print("\nSpectral Type")
print(result_table['SP_TYPE'])


# #### 1.3 - Affichage de la cible au sein d'un widget Aladin
# 
# Les tables récupérées précédemment peuvent ensuite être affichées sur le widget Aladin (au centre de la croix ici pour gam Cas). Il est aussi possible de faire une requête sur une région plutôt qu'un object distinct avec "Simbad.query_region", plus d'informations sur son installation et les sources utilisées ici : https://github.com/cds-astro/ipyaladin

# In[6]:


import ipyaladin.aladin_widget as ipyal
aladin = ipyal.Aladin(target='gam cas', fov = 1)
aladin


# Les tables récupérées précédemment peuvent ensuite être affichées sur le widget Aladin (au centre de la croix ici pour gam Cas). Il est aussi possible de faire une requête sur une région plutôt qu'un object distinct avec "Simbad.query_region"

# In[7]:


#Show tables on Aladin widget
aladin.add_table(result_table)

#record render in jpeg
aladin.get_JPEG_thumbnail()


# Un affichage de différents survey permettra également de comparer notre cible.

# In[8]:


from ipywidgets import Layout, Box, widgets

a = ipyal.Aladin(layout=Layout(width='33.33%'), target='gam cas', fov=0.3)
b = ipyal.Aladin(layout=Layout(width='33.33%'), survey='P/DSS2/red')
c = ipyal.Aladin(layout=Layout(width='33.33%'), survey='P/2MASS/color')

# synchronize target between 3 widgets
widgets.jslink((a, 'target'), (b, 'target'))
widgets.jslink((b, 'target'), (c, 'target'))

# synchronize FoV (zoom level) between 3 widgets
widgets.jslink((a, 'fov'), (b, 'fov'))
widgets.jslink((b, 'fov'), (c, 'fov'))

items = [a, b, c]

box_layout = Layout(display='flex',
                    flex_flow='row',
                    align_items='stretch',
                    border='solid',
                    width='100%')
box = Box(children=items, layout=box_layout)
box


# Il existe d'autres modules de visualisation tout aussi intéressants, mais pour ne pas rallonger cet article, voici les liens officiels : 
# - PyESASky : https://www.cosmos.esa.int/web/esdc/pyesasky
# - PyWWT : https://pywwt.readthedocs.io/en/stable/installation.html

# ## Fichiers sources et visualisation des données d'observations

# ### 2 - Manipulation de fichiers FITS avec Astropy
# 
# Documentation officielle et sources utiles : 
# - https://learn.astropy.org/FITS-images.html
# - https://learn.astropy.org/rst-tutorials/FITS-tables.html?highlight=filtertutorials
# 
# #### 2.1 - En-tête FITS et métadonnées

# In[9]:


import warnings #disable warnings for clear output
warnings.filterwarnings("ignore", category=UserWarning)

#Import Mathematics
import numpy as np

# Imports for Visualisation
# matplotlib and use a nicer set of plot parameters
import matplotlib
import matplotlib.pyplot as plt
get_ipython().run_line_magic('config', 'InlineBackend.rc = {}')
get_ipython().run_line_magic('matplotlib', 'notebook')

from astropy.visualization import astropy_mpl_style
plt.style.use(astropy_mpl_style)# set astropy Graph style for plot
from astropy.io import fits #fits manipulation


# In[10]:


#open image and show file info
image_path = 'dataset/gamcas_siril_astrometry.fit'
hdu_list = fits.open(image_path)
hdu_list.info()


# Une fois le fichier FITS chagée en mémoire, il est possible de lire les données brutes, ainsi que le header pour voir les métadonnées.

# In[11]:


hdu = hdu_list[0]
image_data = hdu_list[0].data
image_header = hdu_list[0].header

print(f'Image Data Type : {type(image_data)} - Shape : {image_data.shape}')
print('------------------------------ Header ------------------------------')
print(repr(image_header))
print('---------------------------- End Header ----------------------------')


# #### 2.2 - Visualisation de l'image avec MatplotLib

# Avec différents types de visualisation selon les paramètres lors de l'appel de la métode imshow()

# In[12]:


#Init figure
f1 = plt.figure()
f1.add_subplot()

#Show raw image - Parameters dataset-image & colorMap
plt.imshow(image_data, cmap='gray')
plt.show()


# <b>Caracéristiques de l'image</b>

# Pour afficher des informations concernant les caractéristiques de la prise de vue, cette dernière librairie peut être associée à la librairie numpy. Cela permet ainsi d'afficher les valeurs statistiques (min, max, moy, écart-type) en complément de l'histogramme de l'image.
# 
# Attention, il s'agit ici d'une image en 32 bit à virgule flottante (float). Les valeurs seront bien entendu différentes en travaillant avec une image en 16 bit (int).

# In[13]:


print('----- Statistics values -----')
print('Min :', np.min(image_data))
print('Max :', np.max(image_data))
print('Mean :', np.mean(image_data))
print('Stdev :', np.std(image_data))
print('Data Type :', image_data.dtype) #i.e. <f4 = little-endian single-precision floating point 32 bit 
#(More detail about stype at https://numpy.org/doc/stable/reference/arrays.dtypes.html)
print('Image length : ', len(image_data)) # size list
print('Shape :', image_data.shape) # dimensions of the array


# In[14]:


np.seterr(divide='ignore') #suppress the warnings raised by taking log10 of data with zeros

#New figure
f2 = plt.figure()

#Prepare Histogram
#Because image is a 2D Tab, need to convert in 1-D for plotting
#Use flatten () method on an array return 1-D numpy tab.
plt.hist(np.log10(image_data.flatten()), range=(-3, 2), bins=1000);

#Show Histogram
plt.show()


# On pourra ainsi modifier l'affichage de notre image en fonction des zones de l'histogramme qui nous intéressent le plus (i.e -0.2 / 0.2), car leurs visualisation est facilité.

# In[15]:


f3 = plt.figure()
img = plt.imshow(image_data)
img.set_clim(-0.1,0.3)
plt.colorbar()


# Mais également utiliser différents types de colorations (cmap) et une échelle logarithmique avec numpy ( np.log(dataset-image) ).

# In[16]:


#hide warnings
import warnings
warnings.filterwarnings("ignore", category=UserWarning)

from matplotlib.colors import LogNorm

f4 = plt.figure()
f4.suptitle('gam Cas with different visualisation type')

f4.add_subplot(2,2, 1)
plt.imshow(np.log10(image_data), cmap='plasma', vmin=-3, vmax=0, origin='lower')
plt.colorbar()

f4.add_subplot(2,2, 2)
plt.imshow(np.log10(image_data), cmap='viridis', vmin=-3, vmax=0, origin='lower')
plt.colorbar()

f4.add_subplot(2,2,3)
plt.imshow(np.log10(image_data), cmap='hot', vmin=-3, vmax=0, origin='lower')
plt.colorbar()

f4.add_subplot(2,2, 4)
plt.imshow(np.log10(image_data), cmap='nipy_spectral',  vmin=-3, vmax=0, origin='lower')
plt.colorbar()

plt.show(block=True)

#More params at https://matplotlib.org/3.1.1/tutorials/colors/colormapnorms.html


# ### 3 - Positionnement céleste de l'image 

# Lors du traitement de l'image avec le logiciel Siril (https://www.siril.org/), une résolution astrométrique a été faite et le résultat a été ajouté au header de l'image fit. Il est possible de projeter ces coordonnées sur notre affichage (ax = plt.subplot(projection=wcs_for_plot)) avec Matplotlib après l’instanciation d’un objet WCS du module du même nom provenant d’Astropy. 
# 
# Il est également possible de dessiner les contours des objets, selon les différents niveaux détectés dans la matrice.

# #### 3.1 - Affichage d'un calque de coordonnées via le Header

# In[17]:


get_ipython().run_line_magic('matplotlib', 'inline')
from astropy.wcs import WCS
import numpy as np
from matplotlib.colors import LogNorm
import warnings
warnings.filterwarnings("ignore", category=UserWarning)#delete after also
#warnings.filterwarnings("ignore", category=Warning)#delete after also

#Create WCS object
wcs_for_plot = WCS(image_header)

#prepare plot
fig5 = plt.figure(figsize=(20, 20))
ax = plt.subplot(projection=wcs_for_plot) #add wcs informations on plot
plt.imshow(image_data, origin='lower', cmap='gray', aspect='equal',norm=LogNorm())
plot_title = "Gamma Cassiopae "
plt.title(plot_title ,fontsize=16, loc='left')

#add and configure axis informations
ax = plt.gca()
ax.grid(True, color='white', ls='solid')
ra = ax.coords[0]
dec = ax.coords[1]

ra.set_axislabel('RA (J2000)',  minpad=1)
dec.set_axislabel('DEC (J2000)',  minpad=1)
ra.set_major_formatter('hh:mm:ss.s')
dec.set_major_formatter('dd:mm:ss.s')

#add other grid as overlay
overlay = ax.get_coords_overlay('icrs') # or galactic, alt-az, ...
overlay.grid(True, color='lime', ls='solid')
overlay[0].set_axislabel('RA (deg)')
overlay[1].set_axislabel('DEC (deg)')

#uncomment the line after for show contour on objects
#ax.contour(image_data, transform=ax.get_transform(wcs_for_plot), levels=np.logspace(-3, 0, 3), colors='orange', alpha=1)

#show
plt.show()


# #### 3.2 - Récupération des coordonnées par le service astrometry.net

# Pour pouvoir récupérer les coordonnées correspondant à l'image, une requête au service d'astrométrie (astrometry.net) est effectuée grâce à la librairie AstroQuery.
# 

# <b>Conversion de l'image FITS en JPEG</b>

# L'image initiale faisant 80 Mo, il est possible de convertir cette image FITS en jpeg/png, avant de faire appel au service d'astrometrie, afin d'avoir une image plus légère d'environ 100ko et d'accélérer le processus.

# In[34]:


#image name (FITS) to convert : image_path
#image name convert in jpeg/png : gamcas_jpeg.jpeg
from matplotlib.colors import LogNorm
get_ipython().run_line_magic('matplotlib', 'inline')
fig6 = plt.figure(figsize=(16, 9))
plt.imshow(image_data, cmap='gray', norm=LogNorm())
plt.grid()
plt.axis('off')
plt.savefig("dataset/gamcas_jpeg.jpeg", bbox_inches='tight')


# <b>Requêtage de l'API Astrometry.net</b>

# Il est nécessaire de préciser la clé d'API lié à votre compte. Cette dernière est disponible sur la page de votre compte, à cette adresse : http://nova.astrometry.net/api_help. L'avertissement présent est normal et demande simplement de préciser notre configuration.

# In[20]:


import warnings #disable warnings for clear output
warnings.filterwarnings("ignore", category=Warning)

#Import astroquery/astrometry library and World Coordinate System module from astropy
from astroquery.astrometry_net import AstrometryNet
from astropy.wcs import WCS


ast = AstrometryNet()
ast.api_key = '##############' # PUT YOUR API KEY HERE

try_again = True
submission_id = None

while try_again:
    try:
        if not submission_id:
            wcs_header = ast.solve_from_image('dataset/gamcas_jpeg.jpeg',
                                              submission_id=submission_id)
        else:
            wcs_header = ast.monitor_submission(submission_id,
                                                solve_timeout=120)
    except TimeoutError as e:
        submission_id = e.args[1]
    else:
        # got a result, so terminate
        try_again = False


#when solve is ok, record data in wcs_gamcas object
if wcs_header:
    print('Solve OK - Print wcs result ...')
    wcs_gamcas = WCS(wcs_header)
    print(wcs_gamcas)
    
else:
    print('nok')
      


# <b>Affichage des coordonnées célestes après le retour de l'API Astrometry.net</b>

# Dès lors, les coordonnées peuvent être affichées par-dessus l'image, dans le système de coordonnées souhaité. Plus d'informations sur le système de gestion WCS d'Astropy à ces adresses :
# - https://het.as.utexas.edu/HET/Software/Astropy-1.0/wcs/index.html
# - https://docs.astropy.org/en/stable/visualization/wcsaxes/initializing_axes.html

# In[33]:


import matplotlib.image as mpimg
    
#Prepare plot
img = mpimg.imread('dataset/gamcas_jpeg.jpeg') #Image name_file
fig7 = plt.figure(figsize=(20, 20))

ax = plt.subplot(projection=wcs_gamcas) #Add wcs projection with wcs_gamcas file
plt.imshow(img, origin='lower', cmap='gray', aspect='equal',norm=LogNorm())

plt.title('gam Cas - Astrometry.net API',fontsize=16, loc='center')

ax.coords['ra'].set_axislabel('RA')
ax.coords['dec'].set_axislabel('DEC')

overlay = ax.get_coords_overlay('icrs')
overlay.grid(color='red', ls='dotted')

plt.show()


# <b>Récupération et affichage des coordonnées sans passer par l'API en ligne de code</b>

# Dans l'éventualité où le repérage astrométrique n'est pas effectué par l'API Python via le morceau de code précédent. L'utilisation classique via l'upload de fichier FITS sur le site astrometry.net permet de récupérer le fichier WCS unique, ou l'image FITS incluant l'en-tête WCS. Le code ci-dessous présente l'affichage des coordonnées sur l'image par cette méthode. Le fichier récupéré via le site astrometry.net s'appelle 'wcs.fit'

# In[22]:


#Get wcs file path
wcs_file = fits.open('dataset/wcs.fits')
header_wcs_file = wcs_file[0].header

#Create WCS object
wcs_for_plot2 = WCS(header_wcs_file)

#Image plot must be the same than upload image on astrometry.net
import matplotlib.image as mpimg
img = mpimg.imread('dataset/gamcas_jpeg.jpeg')

fig8 = plt.figure(figsize=(20, 20))
ax = plt.subplot(projection=wcs_for_plot2)
plt.imshow(img, origin='lower', cmap='gray', aspect='equal',norm=LogNorm())

plot_title = "Gamma Cassiopae"
plt.title(plot_title ,fontsize=16, loc='left')

ax.coords['ra'].set_axislabel('RA')
ax.coords['dec'].set_axislabel('DEC')

overlay = ax.get_coords_overlay('icrs')
overlay.grid(color='red', ls='dotted')

plt.show()


# ### 4 - Affichage et manipulation des spectres traités avec SpecUtils
# 
# #### 4.1 - Visualisation du Spectre 2D

# À partir des éléments utilisés précédemment, il est possible donc possible d'afficher le spectre brut acquis et de vérifier qu'il n'est pas saturé

# In[23]:


#open image and show file info
spec1D_path = 'dataset/Navi_Light_007.fits'
hdu_list_spec1D = fits.open(spec1D_path)
hdu_list_spec1D.info()

spec1D_data = hdu_list_spec1D[0].data
spec1D_header = hdu_list_spec1D[0].header

print(f'Image Data Type : {type(spec1D_data)} - Shape : {spec1D_data.shape}')
print('------------------------------ Header ------------------------------')
print(repr(spec1D_header))
print('---------------------------- End Header ----------------------------')

fig9 = plt.figure()
plt.imshow(spec1D_data, cmap='gray',norm=LogNorm(), vmin=1800,vmax = 4500)
plt.grid(False)

print('Statistics values')
print('Min:', np.min(spec1D_data))
print('Max:', np.max(spec1D_data))
print('Mean:', np.mean(spec1D_data))
print('Stdev:', np.std(spec1D_data))


# À noter que la partie de réduction du spectre 2D vers 1D n’est pas évoquée ici. Il est considéré que ce traitement est effectué avec les logiciels courants (i.e. ISIS, Demetra, VSpec, SpcAudace, etc.).
# - ISIS : http://www.astrosurf.com/buil/isis-software.html
# - Demetra : https://www.shelyak.com/logiciel-demetra/
# - VSpec : http://astrosurf.com/vdesnoux/
# - SpcAudace : http://spcaudace.free.fr/

# #### 4.2 - Visualisation du spectre 1D avec SpecUtils et Matplotlib

# In[24]:


#Show a unique spectrum 

#imports
from astropy import units as u #units
import astropy.wcs as fitswcs #wcs
from specutils import Spectrum1D, SpectralRegion #spectrum1D (specutils)

spec_path = "dataset/GamCas_20201104T204413.fit"

#open & load spectrum file
file = fits.open(spec_path)  
specdata = file[0].data
header = file[0].header

#make WCS object
wcs_data = fitswcs.WCS(header={'CDELT1': header['CDELT1'], 'CRVAL1': header['CRVAL1'],
                               'CUNIT1': header['CUNIT1'], 'CTYPE1': header['CTYPE1'],
                               'CRPIX1': header['CRPIX1']})

#set flux units
flux= specdata * u.Jy

#create a Spectrum1D object with specutils
spec = Spectrum1D(flux=flux, wcs=wcs_data)

#plot spectrum
fig, ax = plt.subplots(figsize=(16, 9))
ax.plot(spec.spectral_axis * u.AA, spec.flux)

#X axis label
ax.set_xlabel(header['CTYPE1'] + ' - ' + header['CUNIT1'])

#Y axis label
ax.set_ylabel('Relative Flux')

#Grid configuration
ax.grid(color='grey', alpha=0.8, linestyle='-.', linewidth=0.2, axis='both') 
    
#legend configuraiton
legend_value = header['OBJNAME'] + ' - ' + header['DATE-OBS']
ax.legend([legend_value], loc=('best'))

#prepare and set plot title with header infos 
spectrumTitle = header['OBJNAME'] + ' - ' + header['DATE-OBS'] + ' - '+ header['EXPTIME2']+ ' - ' + str(header['DETNAM'])
ax.set_title(spectrumTitle, loc='center', fontsize=12, fontweight=0.5)

#Show Plot
plt.show()


# #### 4.3 - Normalisation du spectre par son continuum#### 3.3 - Norma

# <b>Affichage du continuum</b>

# In[25]:


#Visualize continuum fitting

#imports
from astropy.modeling import models, fitting
from specutils.fitting import fit_generic_continuum

#prepare data
x = spec.spectral_axis
y = spec.flux

#fitting continuum (with exclude region between 3700A and 4000A)
g1_fit = fit_generic_continuum(spec, exclude_regions=[SpectralRegion(3700 * u.AA, 4000 * u.AA)])
y_continuum_fitted = g1_fit(x)

#make plot
fig10, ax10 = plt.subplots(figsize=(8,5))
#spectrum
ax10.plot(x, y)
#continuum
ax10.plot(x, y_continuum_fitted, color="orange")
ax10.grid(True)
plt.show()


# <b>Division par le continuum</b>

# In[26]:


#Normalization

#prepare data
x_2 = spec.spectral_axis
y_2 = spec.flux

#fitting continuum (with exclude region 3700/4000 & H lines emission)
g_fit = fit_generic_continuum(spec, exclude_regions=[SpectralRegion(3700 * u.AA, 4000 * u.AA), SpectralRegion(4825 * u.AA, 4885 * u.AA), SpectralRegion(6520 * u.AA, 6540 * u.AA)])

#divide spectrum by his continuum
y_cont_fitted = g_fit(x_2)
spec_normalized = spec / y_cont_fitted

#show on plot
fig11, ax11 = plt.subplots(figsize=(8,5))
ax11.plot(spec_normalized.spectral_axis, spec_normalized.flux)
ax11.grid(True)
plt.show()


# #### 3.4  - Détection des raies d'émissions

# Une fois le spectre normalisé, il est possible d'utiliser la méthode find_lines_derivative pour détecter les raies d'émissions, ou d'absorptions dans le spectre. Il existe deux méthodes pour cette étape, par calcul de la dérivée puis seuillage du flux (comme ci-dessous), ou par seuillage du flux en fonction d'un facteur appliqué à l'incertitude spectrale. Plus d'information sur ces méthodes ici : https://specutils.readthedocs.io/en/stable/fitting.html

# In[27]:


#imports
from specutils.fitting import find_lines_derivative
from specutils.fitting import fit_lines

#Line fitting with Derivative technique
lines = find_lines_derivative(spec_normalized, flux_threshold=1.2)
print('emission: ', lines[lines['line_type'] == 'emission']) 


# #### 4.5 - Sélection d'une région spectrale et affichage des valeurs d'analyse des raies

# À partir de la détection des raies en émissions précédentes, la sélection d'une région spectrale associée permet de se concentrer sur une d'entre elles, Halpha par exemple, et d'utiliser les outils d'analyse existant afin de récupérer les valeurs (i.e. centroïde, fhwm, snr, estimation des paramètres d'un modèlé Gaussient, etc..). Plus d'informations sur ces outils d'analyse ici : https://specutils.readthedocs.io/en/stable/fitting.html#parameter-estimation

# In[28]:


#import analysis tools
from specutils.manipulation import extract_region
from specutils.fitting import estimate_line_parameters
from specutils.analysis import centroid, fwhm

#create spectral region (after line detection of 6562.77A) +/- 50A
sr =  SpectralRegion((6563-50)*u.AA, (6563+50)*u.AA)

#print centroid - need a spectrum and a spectral region in parameters
center = centroid(spec_normalized, sr)  
print("center : ", center)

#print fwhm - need a spectrum and a spectral region in parameters
fwhm_spec = fwhm(spec_normalized, regions=sr)
print("fwhm : ", fwhm_spec)

#create a new spectrum of the selected region for plot
sub_spectrum = extract_region(spec_normalized, sr)
Ha_line = Spectrum1D(flux=sub_spectrum.flux,spectral_axis=sub_spectrum.spectral_axis)

#plot
fig12, ax12 = plt.subplots(figsize=(8,5))
ax12.plot(Ha_line.spectral_axis, Ha_line.flux)
ax12.grid(True)
plt.show()


# #### 4.6 - Visualisation multi-spectres

# Lorsque l’on fait l’acquisition de plusieurs spectres dans le temps sur une même cible, il est intéressant de les visualiser sur le même graphique pour pouvoir les comparer. Les portions de codes précédents peuvent ainsi être réutilisées. Pour l’exemple ci-après, j’ai doublé les deux fichiers de spectres de gam Cas fait le même soir pour que ce soit plus visible.

# In[29]:


#imports if needed
from astropy.io import fits
import astropy.units as u
import astropy.wcs as fitswcs
from specutils import Spectrum1D
import matplotlib.pyplot as plt

#list of path files
spec_list = ["dataset/GamCas_20201104T204413.fit",
            "dataset/GamCas_20201104T204413-2.fit",
            "dataset/GamCas_20201104T204937.fit",
            "dataset/GamCas_20201104T204937-2.fit"] 

#list of spectrum1D
spec1D_list = []

#modify offset for change visualization preferences
offset = 0.3

#parse and create plot for each spectrum in progress
for sip in spec_list :
    sip_file = fits.open(sip)
    sip_data = sip_file[0].data
    sip_header = sip_file[0].header

    #create WCS object for each spectrum in progress
    sip_wcs = fitswcs.WCS(header={'CDELT1': sip_header['CDELT1'], 'CRVAL1': sip_header['CRVAL1'],
                                   'CUNIT1': sip_header['CUNIT1'], 'CTYPE1': sip_header['CTYPE1'],
                                   'CRPIX1': sip_header['CRPIX1']})
    
    #apply offset on flux and create spec1D
    sip_flux = (sip_data + (offset * (spec_list.index(sip)))) * u.Jy
    sip_spec1D = Spectrum1D(flux=sip_flux, wcs=sip_wcs)
    
    #add spec1D to spec1DList
    spec1D_list.append((sip_spec1D, sip_header))
        
        
#create a fig
fig = plt.subplots(figsize=(16, 9))

#create a line plot for each spec1D in the list
for spec1Dip in spec1D_list:
    plt.plot(spec1Dip[0].spectral_axis, spec1Dip[0].flux, label=spec1Dip[1]["DATE-OBS"])
    
#Plot configuration 
plt.legend(loc='upper right')
plt.xlabel("Wavelenght ($\lambda$) in $\AA$ ")
plt.ylabel("Relative Flux")
plt.title("gamCas - Spectrum Comparaison - Alpy600", fontsize=16, fontweight='bold')
plt.show()


# #### 4.4 - Librairies complémentaires

# Le code pour l'implémentation de la librarie Bokeh (voir specBok) se trouve à cette adresse :
# 
# https://gitlab.com/chronosastro/aspyt
#     
# Un exemple sera ajouté dans ce notebook plus tard.

# ------

# Merci d'avoir pris le temps de lire jusqu'ici ! :)
# 
# Contact : https://stellartrip.net/contact

# ##### Sources et informations complémentaires (update in progress)

# <sup>1</sup> http://www.astrosurf.com/buil/isis-software.html
# 
# <sup>2</sup> https://www.shelyak.com/logiciel-demetra/
# 
# <sup>3</sup> http://astrosurf.com/vdesnoux/
# 
# <sup>4</sup> https://jupyter.org/
# 
# <sup>5</sup> https://fr.wikipedia.org/wiki/Markdown
# 
# <sup>6</sup> https://en.wikipedia.org/wiki/Astropy
# 
# <sup>7</sup> https://www.astropy.org/
# 
# <sup>8</sup> http://simbad.u-strasbg.fr/simbad/
# 
# <sup>9</sup> https://learn.astropy.org/FITS-images.html
# 
# <sup>10</sup> https://matplotlib.org/
# 
# <sup>11</sup> https://learn.astropy.org/rst-tutorials/FITS-tables.html?highlight=filtertutorials
# 
# <sup>12</sup> https://www.siril.org/
# 
# <sup>13</sup> https://docs.astropy.org/en/stable/visualization/wcsaxes/ticks_labels_grid.html
# 
# <sup>14</sup> http://spcaudace.free.fr/
# 
# <sup>15</sup> https://specutils.readthedocs.io/en/stable/index.html

# In[ ]:




